'use strict';

/**
 * @ngdoc function
 * @name nectanTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the nectanTestApp
 */
angular.module('nectanTestApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
