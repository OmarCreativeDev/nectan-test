'use strict';

/**
 * @ngdoc function
 * @name nectanTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nectanTestApp
 */
angular.module('nectanTestApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
