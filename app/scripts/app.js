'use strict';

/**
 * @ngdoc overview
 * @name nectanTestApp
 * @description
 * # nectanTestApp
 *
 * Main module of the application.
 */
angular
  .module('nectanTestApp', [
    'ngAnimate',
    'ngRoute',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
